[[_TOC_]]

# Initiation à Git
Logiciel de versionning.

## Initialiser le projet
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-init/en)

Crée un dépôt Git vide ou réinitialise un dépôt existant. Génère un dossier caché `.git`.

```bash
$ git init
```
## Configurations
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-config)

### Régler l'identité du compte

```bash
$ git config --global user.email "bibi@mail.com"
$ git config --global user.name "username"
```
Ces informations sont stockées dans le fichier `.gitconfig` du home (cd ~). Éliminez `--global` pour ne faire les réglages que dans le dépôt du projet.
### Les alias
[![french link](img/icons/link.png)_doc fr_](https://git-scm.com/book/fr/v2/Les-bases-de-Git-Les-alias-Git)

#### Créer un alias

```bash
# Cet alias confgure une autre manière de visualiser les log (plus synthétique)
$ git config --global alias.logmin 'log --pretty=format:"%H%x09%an%x09%ai%x09%s"'
# Ici, on execute l'alias
$ git loginmin
33ed3c6b5eff55d70691cb55b38b2423cc8eba26        mathieugodard   2022-07-15 20:20:58 +0200       Ajout de la balise <base> dans le head des pages HTML
```
#### Lister les alias
```bash
$ git config --get-regexp alias
```

## Fichier .gitignore
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/gitignore)

Il faut créer un fichier caché `.gitignore` à la racine du projet, là où a été créé l'initialisation du dépôt git.

```bash
$ touch .gitignore
```

Ce fichier sert à exclure des fichiers et/ou dossiers, lors des envois vers les dépôts distants. Par exemple les dossiers `dist`, `node_modules`, ou le fichier `.gitignore` lui-même !


 Il est possible de générer une partie du fichier de manière automatique, par exemple à l'aide du site [toptal](https://www.toptal.com/developers/gitignore), en fonction du projet.


## Afficher la différence entre le dépôt local et le dépôt distant
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-status/en)

```bash
$ git status
```
## Préparer l'envoi vers le dépôt distant
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-add)

```bash
$ git add . # Envoi tous les éléments indiqués par le status 
```
```bash
$ git add readme.md index.html # Envoi seulement ces éléments
```
```bash
git add -p # Voir les modifications
```
## Enregistrer les modifications dans le dépôt
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-commit)

Envoi sur le dépôt (le -m c’est pour ajouter un message) :
```bash
$ git commit -m "Le super commentaire pour comprendre quoi qu'on a fait"
```

## Voir les logs de chaque commit
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-log)

```bash
$ git log
commit 548ce96ba53544f556a6b81eb98bc204b89159b3 (HEAD -> master, origin/master)
Author: MathieuGodard <mathieu.godard@gmail.com>
Date:   Thu Oct 1 10:42:30 2020 +0200

Premier commit
```

## Mise à jour du dépôt distant
[![english link](img/icons/link.png)_doc en_](https://git-scm.com/docs/git-push)

```bash
$ git push
```

## Ajouter un autre repo lors du push
Source : https://heitorpb.github.io/bla/2020/08/11/git-multiple-remotes/

There is a simple way to set up git to use multiple push URLs for a single remote. This way you can easily mirror any git repository into any hosting provider when git pushing.

Let’s say your GitHub’s user is user and you want to mirror your repo into GitLab. Your local clone is initially:
```bash
$ git remote -v
origin  git@github.com:user/repo.git (fetch)
origin  git@github.com:user/repo.git (push)
```
Now we need to configure the origin remote to have two URLs:
```bash
$ git remote set-url --add --push origin git@github.com:user/repo.git
$ git remote set-url --add --push origin git@gitlab.com:user/repo.git
```
And the magical result:
```bash
$ git remote -v
origin  git@github.com:user/repo.git (fetch)
origin  git@github.com:user/repo.git (push)
origin  git@gitlab.com:user/repo.git (push)
```
This makes every git push send the changes to both GitHub and GitLab, creating an automatic backup.

But note that git pull is still from GitHub. It is not possible to pull changes from two different repositories.


# Gitlab

Commandes indiquées par framagit après la création du projet `monprojet`.

## Create a new repository

```bash
git clone git@framagit.org:mathieugodard/monprojet.git
cd monprojet
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

## Push an existing folder

```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@framagit.org:mathieugodard/monprojet.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

## Push an existing Git repository

```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@framagit.org:mathieugodard/monprojet.git
git push -u origin --all
git push -u origin --tags
``` 






















# Gitlab

## Générer une paire de clés SSH
> SSH (Secure Shell) est un protocole de réseau cryptographique qui permet aux administrateurs de contrôler un système à distance. Les clés SSH offrent un moyen plus sûr de se connecter à un serveur avec SSH que d’utiliser un mot de passe seul. Alors qu’un mot de passe peut éventuellement être cracké avec une attaque par force brute, les clés SSH sont presque impossibles à déchiffrer par la seule force brute.
Extrait du site [malekal.com](https://www.malekal.com/generer-et-se-connecter-en-ssh-avec-des-cles-ssh/).

→ créer une paire de clés SSH (publique et privée) et l'associer à son logiciel de forge (compte [Github](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) ou [Gitlab](https://docs.gitlab.com/ee/user/ssh.html) par exemple). Dans la suite du document, c'est Gitlab qui sera utilisé.

```bash
$ ssh-keygen -t ed25519 -C "SSH-Framagit"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/mathieu/.ssh/id_ed25519): # Garder le chemin par défaut
Enter passphrase (empty for no passphrase): # Un mot de passe renforce la sécurité
Enter same passphrase again: 
Your identification has been saved in /home/mathieu/.ssh/id_ed25519
Your public key has been saved in /home/mathieu/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:2DEa852A5DgZz77atrENqvQaVhuQUM7Cb8tTlJVsg1w SSH-Framagit
The key's randomart image is:
+--[ED25519 256]--+
|+o. o BE.        |
|o+ . & *         |
|. + = O =        |
|   + + B = .     |
|  o = + S o      |
|   = o .         |
|  + o +          |
| o o +.=         |
|  oo+.+..        |
+----[SHA256]-----+
```
Le fichier id_ed25519 (clé privée) ne doit pas bouger de son emplacement.
Le fichier id_ed25519.pub (clé publique) doit être collée dans le serveur.

## Ajouter la clé publique au compte Gitlab
Permet d'associer sa machine au compte. Equivalent [Github](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account).
