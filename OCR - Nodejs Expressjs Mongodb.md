Réalisation du backend du cours OpenClassRooms intitulé "[Passez au Full Stack avec Node.js, Express et MongoDB](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb)"

[[_TOC_]]

# Partie 1 - Créez un serveur Express simple

## Configurez votre environnement de développement

Installer node : https://nodejs.org/en/

## Démarrez votre serveur Node

### Initialiser Git

```bash
$ git init
```

### Ajouter le dépôt distant

```bash
$ git remote add origin git@framagit.org:mathieugodard/openclasrooms-node.js-express-mongodb.git
```

### Créer le fichier .gitignore

Y ajouter le dossier node_modules qui sera créé par la suite :

```bash
node_modules
```

### Initialiser le projet

```bash
$ npm init
```

La configuration par défaut a été conservée, à l'exception du point d'entrée que nous nommons `server.js` : fichier que nous créons à la racine du projet.

A l'intérieur de ce fichier, nous ajoutons le code :

```javascript
// server.js

const http = require("http");

const server = http.createServer((req, res) => {
    res.end("Voilà la réponse du serveur !");
});

server.listen(process.env.PORT || 3000);
```

-   On importe le package [`http`](https://nodejs.org/api/http.html) natif de Node (en [CommonJS](https://nodejs.org/api/modules.html#modules-commonjs-modules) à l'aide du mot clé `require`).

-   On utilise ensuite la méthode [`createServer`](https://nodejs.org/api/http.html#httpcreateserveroptions-requestlistener) qui prend en argument deux objets : la requête et la réponse. Ici, la méthode `end` se charge de renvoyer une réponse de type `string` à l'appelant.

-   Enfin, on utilise la méthode `listen` qui permet de configurer le serveur afin d'écouter :
    -   soit la variable d'environnement du port grâce à process.env.PORT : si la plateforme de déploiement propose un port par défaut, c'est celui-ci qu'on écoutera;
    -   soit le port 3000, ce qui nous servira dans le cas de notre plateforme de développement.

### Lancer le serveur

```bash
$ node server
```

On peut désormais accéder à http://localhost:3000 dans une fenêtre de navigation.

Pour voir le résultat de la requête, utiliser Postman ou Insomnia, par exemple.

### Nodemon

Nodemon surveille les modifications de fichiers et redémarre le serveur automatiqument, afin de fournir une version toujours actualisée.

Pour l'installer :

```bash
$ sudo npm install -g nodemon
```

Pour lancer Nodemon :

```bash
$ nodemon server
```

## Créez une application Express

### Installer Express

```bash
$ npm install express
```

P.S. L'option `--save` n'est plus obligatoire depuis la version 5 de NPM : `npm install <package>` ajoute automatiquement le paquet dans les dépendances du fichier `package.json` ([Source](https://expressjs.com/fr/starter/installing.html)).

### Créer l'application

Créer le fichier app.js. C'est dans ce fichier que nous plaçons Express :

-   On importe le module Express
-   On créé l'application à l'aide de la méthode `express()`
-   Enfin, on exporte l'application pour pouvoir l'importer dans le serveur node.

```javascript
// app.js

const express = require("express");

const app = express();

app.use((req, res) => {
    res.json({ message: "Votre requête a bien été reçue !" });
});

module.exports = app;
```

### Exécutez l'application Express sur le serveur Node

```javascript
// server.js

const http = require("http");
const app = require("./app");

app.set("port", process.env.PORT || 3000);
const server = http.createServer(app);

server.listen(process.env.PORT || 3000);
```

### Les middlewares

> Une application Express est fondamentalement une série de fonctions appelées middleware. Chaque élément de middleware reçoit les objets request `req` et response `res`, peut les lire, les analyser et les manipuler, le cas échéant. Le middleware Express reçoit également la méthode next, qui permet à chaque middleware de passer l'exécution au middleware suivant. [Source](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb/6466277-creez-une-application-express#/id/r-6466248)

> Les fonctions de middleware sont des fonctions qui peuvent accéder à l’objet Request `req`, l’objet response `res` et à la fonction middleware suivant dans le cycle demande-réponse de l’application (couramment désignée par une variable nommée `next`). [Source](https://expressjs.com/fr/guide/writing-middleware.html)

[![Schema d’un appel de fonction middleware](img/OCR-Nodejs_Expressjs_Mongodb/express-middleware.png "Schema d’un appel de fonction middleware")_Source : https://expressjs.com_](https://expressjs.com/fr/guide/writing-middleware.html)

Documentation d'Express sur les middleware : [Ecriture de middleware utilisable dans les applications Express](https://expressjs.com/fr/guide/writing-middleware.html) et [Utilisation de middleware](https://expressjs.com/fr/guide/using-middleware.html)

Amélioration du fichier server.js :

```js
// server.js

const http = require("http");
const app = require("./app");

const normalizePort = (val) => {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
};
const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

const errorHandler = (error) => {
    if (error.syscall !== "listen") {
        throw error;
    }
    const address = server.address();
    const bind =
        typeof address === "string" ? "pipe " + address : "port: " + port;
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges.");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use.");
            process.exit(1);
            break;
        default:
            throw error;
    }
};

const server = http.createServer(app);

server.on("error", errorHandler);
server.on("listening", () => {
    const address = server.address();
    const bind =
        typeof address === "string" ? "pipe " + address : "port " + port;
    console.log("Listening on " + bind);
});

server.listen(port);
```

-   la fonction normalizePort() renvoie un port valide, qu'il soit fourni sous la forme d'un numéro ou d'une chaîne ;
-   la fonction errorHandler() recherche les différentes erreurs et les gère de manière appropriée. Elle est ensuite enregistrée dans le serveur ;
-   un écouteur d'évènements est également enregistré, consignant le port ou le canal nommé sur lequel le serveur s'exécute dans la console.

## Créez une route GET

On remplace le middleware en place par le suivant :

```js
// app.js

app.use('/api/stuff', (req, res, next) => {
  const stuff = [
    {
      _id: 'oeihfzeoi',
      title: 'Mon premier objet',
      description: 'Les infos de mon premier objet',
      imageUrl: 'https://cdn.pixabay.com/photo/2019/06/11/18/56/camera-4267692_1280.jpg',
      price: 4900,
      userId: 'qsomihvqios',
    },
    {
      _id: 'oeihfzeomoihi',
      title: 'Mon deuxième objet',
      description: 'Les infos de mon deuxième objet',
      imageUrl: 'https://cdn.pixabay.com/photo/2019/06/11/18/56/camera-4267692_1280.jpg',
      price: 2900,
      userId: 'qsomihvqios',
    },
  ];
  res.status(200).json(stuff);
});
```      
[![Schema d’un appel de fonction middleware](img/OCR-Nodejs_Expressjs_Mongodb/express-middleware.png "Schema d’un appel de fonction middleware")*Source : https://expressjs.com*](https://expressjs.com/fr/guide/writing-middleware.html)

Côté frontend en revanche, la console nous indique un problème :
> `Cross-Origin Request Blocked: The Same Origin Policy disallows reading the remote resource at http://localhost:3000/api/stuff. (Reason: CORS request did not succeed). Status code: (null).`

![Message d'erreur CORS](img/OCR-Nodejs_Expressjs_Mongodb/frontend-CORS.png "Schema d’un appel de fonction middleware")

Qu'est-ce que le CORS ?
> Le «  Cross-origin resource sharing » (CORS) ou « partage des ressources entre origines multiples » (en français, moins usité) est un mécanisme qui consiste à ajouter des en-têtes HTTP afin de permettre à un agent utilisateur d'accéder à des ressources d'un serveur situé sur une autre origine que le site courant. Un agent utilisateur réalise une requête HTTP multi-origine (cross-origin) lorsqu'il demande une ressource provenant d'un domaine, d'un protocole ou d'un port différent de ceux utilisés pour la page courante. [Source MDN](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS)

Pour résoudre le souci, il faut donc rajouter des en-tête à la réponse. On implémente donc CORS en ajoutant un middleware qui sera général (pas de route spécifée) :
```js
// app.js

const express = require('express');
const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

...
````

Ces headers permettent :

-   d'accéder à notre API depuis n'importe quelle origine ( '\*' ) ;
-   d'ajouter les headers mentionnés aux requêtes envoyées vers notre API (Origin, X-Requested-With, etc.) ;
-   d'envoyer des requêtes avec les méthodes mentionnées ( GET, POST, etc.).

## Créez une route POST

La base de données n'est pas encore créée. Par conséquent, nous devons gérer les requêtes POST provenant de l'application frontend :

```js
// app.js

app.use(express.json());
```

-   Express prend toutes les requêtes qui ont comme Content-Type application/json et met à disposition leur body directement sur l'objet req

On peut alors écrire le middleware POST suivant :

```js
// app.js

app.post("/api/stuff", (req, res, next) => {
    console.log(req.body);
    res.status(201).json({
        message: "Objet créé !",
    });
});
```

Le code HTTP 201 signifie que la création de données est un succès.

On peut donc observer l'objet créé dans le formulaire et envoyé avec la méthode post, dans la console de Node.

```bash
[nodemon] starting `node server.js`
Listening on port 3000
{
  title: 'wsdf',
  description: 'vjhkjjkj',
  price: 50000,
  imageUrl: 'hjljl.png',
  _id: '1659449552876',
  userId: 'userID40282382'
}
```

# Partie 2 - Créez une API RESTful

> MongoDB est une base de données NoSQL. Cela signifie que l'on ne peut pas utiliser SQL pour communiquer avec. Les données sont stockées comme des collections de documents individuels décrits en JSON (JavaScript Object Notation). [...] Les avantages principaux de MongoDB sont son évolutivité et sa flexibilité. Le site officiel décrit MongoDB comme étant "construit pour des personnes qui construisent des applications Internet et des applications métier qui ont besoin d'évoluer rapidement et de grandir élégamment". [Source : openclassrooms.com](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb/6466348-configurez-votre-base-de-donnees#/id/r-7446787)

## Configurez MongoDB Atlas

-   Prendre un compte gratuit sur le site [MongoDB Atlas](https://www.mongodb.com/fr-fr/atlas/database).
-   Onglet Database Access : si ce n'est pas fait lors de l'inscription, ajouter un utilisateur en authorisant la lecture et l'écriture de toutes les bases de données.
-   Onglet Network Access : cliquer sur _Add IP Adress_ et autoriser l'accès depuis n'importe où (Add access from Anywhere).

## Connectez votre API à votre cluster MongoDB

Cliquer sur _Connect_ :
![Connexion au cluster](img/OCR-Nodejs_Expressjs_Mongodb/mongodb-connect.png)

Cliquer sur _Connect your application_ :
![Connexion au cluster](img/OCR-Nodejs_Expressjs_Mongodb/mongodb-1-setup-connexion-security.png)

Cliquer sur la dernière version de Node.js et copier la chapine de caractère :
![Connexion au cluster](img/OCR-Nodejs_Expressjs_Mongodb/mongodb-2-choose-a-connection-method.png)

```
mongodb+srv://mathieugodard:<password>@cluster0.ilm7c.mongodb.net/?retryWrites=true&w=majority
```

### Installer Mangoose

Ensuite, il faut installer Mangoose sur notre application backend.

> Mongoose est un package qui facilite les interactions avec notre base de données MongoDB. Il nous permet de :
>
> -   valider le format des données ;
> -   gérer les relations entre les documents ;
> -   communiquer directement avec la base de données pour la lecture et l'écriture des documents.
>
> Tout cela nous permet de dépasser plusieurs des obstacles que l'on peut rencontrer avec des bases de données NoSQL, et d'appliquer nos connaissances en JavaScript à une base encore plus fonctionnelle !
>
> [Source : Open Class Rooms](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb/6466348-configurez-votre-base-de-donnees#/id/r-6596910)

Pour l'installer :

```bash
$ npm install mongoose
```

Importer Mangoose dans l'application, et ajouter la ligne suivante en remplaçant l'adresse SRV et en modifiant `<PASSWORD>` par le mot de passe choisit :

```js
// app.js

const mongoose = require("mongoose");

mongoose
    .connect(
        "mongodb+srv://mathieugodard:<PASSWORD>@cluster0.ilm7c.mongodb.net/?retryWrites=true&w=majority",
        { useNewUrlParser: true, useUnifiedTopology: true }
    )
    .then(() => console.log("Connexion à MongoDB réussie !"))
    .catch(() => console.log("Connexion à MongoDB échouée !"));
```

Dans la console Node.js, on peut alors observer :

```bash
[nodemon] restarting due to changes...
[nodemon] starting `node server.js`
Listening on port 3000
Connexion à MongoDB réussie !
```

La connexion à la base de donnée est confirmée !

## Créez un schéma de données

Pour créer un modèle, nous allons dans un premier temps créer un schéma. Celui-ci sera créé par la méthode `Schema()` de Mangoose, et permettra d'établir, par exemple, le typage et le caractère requis de chaque champ. L'id n'est pas présent car il est généré automatiquement pas MongoDB.

Enfin, on exporte ce schéma en tant que modèle utilisable grâce à la méthode `model()` de Mangoose, en lui passant en argument, le nom du modèle et le schéma qui lui correspond (définit juste avant).

```js
// models/Thing.js

const mongoose = require("mongoose");

const thingSchema = mongoose.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    imageUrl: { type: String, required: true },
    userId: { type: String, required: true },
    price: { type: Number, required: true },
});

module.exports = mongoose.model("Thing", thingSchema);
```

## Enregistrez et récupérez des données

Dans ce chapître, il est question de modifier les méthodes `post()` et `get()`en les liant à la base de données.

### Enregistrement des Things dans la base de données

On commence par importer le modèle :

```js
// app.js

const Thing = require("./models/Thing");
```

Puis on modifie la méthode `post()` :

```js
// app.js

app.post("/api/stuff", (req, res, next) => {
    delete req.body._id;
    const thing = new Thing({
        ...req.body,
    });
    thing
        .save()
        .then(() => res.status(201).json({ message: "Objet enregistré !" }))
        .catch((error) => res.status(400).json({ error }));
});
```

-   Dans le body de la requête, se trouvent tous les champs à transmettre par la méthode `post()`. Mais le champ `_id` sera généré automatiquement pas MongoDB : à l'aide du mot clé `delete`, on supprime donc ce champ, qui est prévu dans la construction du frontend fournit pour ce cours.
-   On instancie le modèle `Thing` et au lieu de récupérer champ par champ, on utilise le [spread operator](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Spread_syntax) pour récupérer toutes les données du body de l'objet requête.
-   Pour enregistrer la donnée en base de données, on utilise la méthode `save()` qui va retourner une promise (pour éviter une expiration de la requête, il faut renvoyer une réponse au frontend) : on attribut alors un statut et on parse la réponse ou l'erreur avec la méthode `json()` dans chacune des méthodes `then` et `catch`.

Aucune erreur n'est observée ni dans la console du navigateur ni dans la console node. Pour vérifier, modifions la méthode get !

### Récupération de la liste de Things en vente

```js
// app.js

app.get("/api/stuff", (req, res, next) => {
    Thing.find()
        .then((things) => res.status(200).json(things))
        .catch((error) => res.status(400).json({ error }));
});
```

On utilise la méthode find qui retourne une promise : un tableau de tous nos objets avec la méthode `then()` et l'erreur dans la méthode `catch()`.
Pour afficher un article seul, la maéthode `get()` utilisée est différente.

### Récupération d'un Thing spécifique

```js
// app.js

app.get("/api/stuff/:id", (req, res, next) => {
    Thing.findOne({ _id: req.params.id })
        .then((thing) => res.status(200).json(thing))
        .catch((error) => res.status(404).json({ error }));
});
```

-   Le params `:id` est un paramètre dynamique dans la route.
-   On utilise cette fois la méthode `findOne()` pour trouver le même item qui à l'`_id` du params de la requête.
-   Ensuite, on fait comme précédemment.

## Modifiez et supprimez des données

### Mettez à jour un Thing existant

```js
// app.js

app.put("/api/stuff/:id", (req, res, next) => {
    Thing.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then(() => res.status(200).json({ message: "Objet modifié !" }))
        .catch((error) => res.status(400).json({ error }));
});
```

-   Ajouter une route avec la méthode `put()`
-   On utilise la méthode `updateOne()` pour modifier dans la base de données
-   Le premier argument c'est l'objet à modifier : on utilise les params (les paramètres de requête)
-   Le deuxième argument c'est la nouvelle version de l'objet : on utilise encore le spread opérator pour récupérer tout l'objet qui se trouve dans le corps de la requête, et on reprécise que l'`_id` (celui des params) qui doit rester inchangé, et qui ne doit donc pas être généré automatiquement par MongoDB.

    _NOTE PERSONNELLE : il n'est pas nécessaire de rajouter `_id: req.params.id` contrairement à ce qui est dit (voir doc [Mongoose](https://mongoosejs.com/docs/api.html#model_Model-updateOne))._

-   Ensuite, on fait comme précédemment.

### Suppression d'un Thing

```js
// app.js

app.delete("/api/stuff/:id", (req, res, next) => {
    Thing.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: "Objet supprimé !" }))
        .catch((error) => res.status(400).json({ error }));
});
```

# Partie 3 - Mettez en place un système d'authentification sur votre application

## Optimisez la structure du back-end

### Configurez le routage

On commence par déporter la logique de routing (`post()`, `get()`, ...) : on créé un dossier appelé `routes`, dans lequel on va créer un fichier `stuff.js`. Cela a pour but de dissocier la logique globale et la logique de routing.

A l'intérieur de ce fichier, on va créer un router qu'on va importer de l'application Express.

> La méthodeexpress.Router() vous permet de créer des routeurs séparés pour chaque route principale de votre application – vous y enregistrez ensuite les routes individuelles. [Open Class Rooms](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb/6466459-optimisez-la-structure-du-back-end#/id/r-7548540)

On importe le modèle `Thing` et on y colle toute la logique de route qui était présente dans le fichier `app.js`.

`app.post` se transforme en `router.post` (et ainsi de suite).

On change également les routes puisque le `/api/stuff` sera géré dans le fichier `app.js`.

Enfin, on exporte `router`.

```js
// routes/stuff.js

const express = require("express");
const router = express.Router();

const Thing = require("../models/Thing");

router.post("/", (req, res, next) => {
    delete req.body._id;
    const thing = new Thing({
        ...req.body,
    });
    thing
        .save()
        .then(() => res.status(201).json({ message: "Objet enregistré !" }))
        .catch((error) => res.status(400).json({ error }));
});

router.put("/:id", (req, res, next) => {
    Thing.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then(() => res.status(200).json({ message: "Objet modifié !" }))
        .catch((error) => res.status(400).json({ error }));
});

router.delete("/:id", (req, res, next) => {
    Thing.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: "Objet supprimé !" }))
        .catch((error) => res.status(400).json({ error }));
});

router.get("/:id", (req, res, next) => {
    Thing.findOne({ _id: req.params.id })
        .then((thing) => res.status(200).json(thing))
        .catch((error) => res.status(404).json({ error }));
});

router.get("/", (req, res, next) => {
    Thing.find()
        .then((things) => res.status(200).json(things))
        .catch((error) => res.status(400).json({ error }));
});

module.exports = router;
```

Enfin, dans notre `app.js`, on supprime l'import du modèle `Thing` puisqu'on s'en sert plus, on importe ce router pour lui donner la route de base du fichier `stuff.js` et lui fournir l'import.

```js
// app.js

const stuffRoutes = require("./routes/stuff");

// Puis, juste avant l'export :
app.use("/api/stuff", stuffRoutes);
```

### Configurez les contrôleurs

Pour cette partie, il est question de séparer la logique métier et la logique de routing, à l'aide de controleurs.

> Un fichier de contrôleur exporte des méthodes qui sont ensuite attribuées aux routes pour améliorer la maintenabilité de votre application. [Open Class Rooms](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb/6466459-optimisez-la-structure-du-back-end#/id/r-7548540)

On créé un dossier `controllers` et à l'intérieur, un fichier `stuff.js` et on y colle toute la logique métier que l'on exporte.

```js
// controllers/stuff.js

const Thing = require("../models/Thing");

exports.createThing = (req, res, next) => {
    delete req.body._id;
    const thing = new Thing({
        ...req.body,
    });
    thing
        .save()
        .then(() => res.status(201).json({ message: "Objet enregistré !" }))
        .catch((error) => res.status(400).json({ error }));
};

exports.modifyThing = (req, res, next) => {
    Thing.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then(() => res.status(200).json({ message: "Objet modifié !" }))
        .catch((error) => res.status(400).json({ error }));
};

exports.deleteThing = (req, res, next) => {
    Thing.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: "Objet supprimé !" }))
        .catch((error) => res.status(400).json({ error }));
};

exports.getOneThing = (req, res, next) => {
    Thing.findOne({ _id: req.params.id })
        .then((thing) => res.status(200).json(thing))
        .catch((error) => res.status(404).json({ error }));
};

exports.getAllThings = (req, res, next) => {
    Thing.find()
        .then((things) => res.status(200).json(things))
        .catch((error) => res.status(400).json({ error }));
};
```

Ainsi, le fichier de routing de `stuff` est beaucoup plus clair et a une sémantique plus expressive. La logique métier est alors séparée de la logique de routing.

```js
// routes/stuff.js

const express = require("express");
const router = express.Router();

const stuffCtrl = require("../controllers/stuff");

router.post("/", stuffCtrl.createThing);
router.put("/:id", stuffCtrl.modifyThing);
router.delete("/:id", stuffCtrl.deleteThing);
router.get("/:id", stuffCtrl.getOneThing);
router.get("/", stuffCtrl.getAllThings);

module.exports = router;
```
## Préparez la base de données pour les informations d'authentification

### Comprenez le stockage de mot de passe sécurisé
Pour stocker les mots de passe, on va les cypter à l'aide du package `bcrypt` (un algorythme unidirectionnel). Le mot de passe sera crypter sous forme de hash dans la base de données, mais aussi un autre lorsque le mot de passe sera saisi pour la connexion utilisateur. Une comparaison sera réalisée, permettant l'identification.
### Créez un modèle de données

On installe une extension Mongoose pour valider le caractère unique de l'email :
```bash
$ npm install mongoose-unique-validator
```
`mongoose-unique-validator` améliore les messages d'erreur lors de l'enregistrement de données uniques.

On créé un modèle comme précédemment, mais pour le User :
```js
// models/User.js

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);
```

## Créez des utilisateurs
### Configurez les routes d'authentification
Dans la même logique que pour les `stuff`, alors qu'on vient de créer le modèle, on créé la logique métier et la logique de routing.

La logique métier dans un controleur :
```js
// controllers/user.js

exports.signup = (req, res, next) => {

};

exports.login = (req, res, next) => {

};
```
La logique de routing dans un dossier de routing :
```js
// routes/user.js

const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/user');

router.post('/signup', userCtrl.signup);
router.post('/login', userCtrl.login);

module.exports = router;
```

Et ne pas oublier d'inclure ces routes dans la logique globale du fichier `app.js` : on importe au début du fichier et on implémente juste avant d'exporter l'application.

```js
// app.js

const userRoutes = require('./routes/user');
```
```js
// app.js

app.use('/api/auth', userRoutes);
```

### Créez des utilisateurs

La logique métier : on importe bien entendu le modèle `User` et le package `bcrypt` pour hasher le mot de passe de manière asynchrone (grâce à la méthode `hash` "salé" 10 fois), afin de créer l'utilisateur avec le `hash` reçu en promise, et de le sauvegarder dans la base de données :

```js
// controllers/user.js

const User = require("../models/User");
const bcrypt = require("bcrypt");

exports.signup = (req, res, next) => {
    bcrypt
        .hash(req.body.password, 10)
        .then((hash) => {
            const user = new User({
                email: req.body.email,
                password: hash,
            });
            user.save()
                .then(() =>
                    res.status(201).json({ message: "Utilisateur créé !" })
                )
                .catch((error) => res.status(400).json({ error }));
        })
        .catch((error) => res.status(500).json({ error }));
};
```
Consulter la [Doc bcrypt](https://github.com/kelektiv/node.bcrypt.js) ou directement sa [Doc API](https://github.com/kelektiv/node.bcrypt.js#API)

### Implémentez la fonction login

Logique métier : on utilise la méthode `findOne()` sur la classe `User` pour chercher un document dans la base de données. Dans le premier argument, on lui passe l'objet qui va servir de filtre : le champ email transmis pas le client. C'est une promesse, donc on utilise les méthodes `then()` et `catch()`. Les erreurs reçuent sont liées aux erreurs d'execution dans la base de données.

Ensuite, on teste la valeur trouvée par la requête : si elle est nulle, on envoie une erreur 401 qu'il faut garder floue (ne pas donner d'indice sur le problème). Si la valeur n'est pas nulle, alors on compare le mot de passe envoyé par l'utilisateur avec le mot de passe hashé en base de données, grâce à la méthode `compare()`. Si la valeur retournée est `false`, alors on retourne le même message d'erreur. Dans le cas contraire (valeur retournée `true`), alors on retourne un objet qui contient les informations d'authentification des requêtes émisent par le client : le user._id, et le TOKEN (codé en dur, dans une chaîne de caractère pour le moment, mais que nous allons générer dans le chapitre suivant, pour authentifier nos requêtes).

```js
// controllers/user.js

exports.login = (req, res, next) => {
   User.findOne({ email: req.body.email })
       .then(user => {
           if (!user) {
               return res.status(401).json({ message: 'Paire login/mot de passe incorrecte'});
           }
           bcrypt.compare(req.body.password, user.password)
               .then(valid => {
                   if (!valid) {
                       return res.status(401).json({ message: 'Paire login/mot de passe incorrecte' });
                   }
                   res.status(200).json({
                       userId: user._id,
                       token: 'TOKEN'
                   });
               })
               .catch(error => res.status(500).json({ error }));
       })
       .catch(error => res.status(500).json({ error }));
};
```

## Créez des tokens d'authentification

> Les tokens d'authentification permettent aux utilisateurs de se connecter une seule fois à leur compte. Au moment de se connecter, ils recevront leur token et le renverront automatiquement à chaque requête par la suite. Ceci permettra au back-end de vérifier que la requête est authentifiée. [Open Class Rooms](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb/6466557-creez-des-tokens-dauthentification#/id/r-6715713)


Installer jsonwebtoken :

```bash
$ npm install jsonwebtoken
```
Importer le package :

```js
// controllers/user.jes

const jwt = required('jsonwebtoken');
```

Enfin, renvoyer le TOKEN en utilisant la méthode `sign()` [voir la doc](https://github.com/auth0/node-jsonwebtoken), qui prend 3 arguments :
- Le payload : les données qu'on veut encoder à l'intérieur de ce TOKEN. On choisit l'identifiant du user.
- La clé secrète pour l'encodage, qu'il faudra complexifier pour la mise en production
- la configuration : en l'occurrence, l'expiration du token.

Nous avons donc dans la requête, un en-tête avec une clé `Authorization` qui fournit le token (en trois parties). Note : "Bearer" veut dire "Porteur".

![Le token dans la requête](img/OCR-Nodejs_Expressjs_Mongodb/header-request-authorization-token.png)

En savoir plus sur JSON Web Token : [Wikipédia](https://fr.wikipedia.org/wiki/JSON_Web_Token).

## Configurez le middleware d'authentification

### Implémentez le middleware d'authentification

Nous créons un dossier `middleware` et créons un fichier `auth.js`, afin de contrôler le token reçu et de l'envoyer à nos route avant d'appeler les controlleurs liées à ces routes.

```js
// middleware/auth.js

const jwt = require('jsonwebtoken');
 
module.exports = (req, res, next) => {
   try {
       const token = req.headers.authorization.split(' ')[1];
       const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
       const userId = decodedToken.userId;
       req.auth = {
           userId: userId
       };
	next();
   } catch(error) {
       res.status(401).json({ error });
   }
};
```

Explication de la logique métier :
- On commence par faire un `try... catch` pour gérer les erreurs. Si l'authentification échoue, alors le catch renvoi l'erreur dans la console avec un statut 401.
- Dans un premier temps, on récupère le token dans la clé `Authorization` de l'en-teête de la requête. C'est la deuxième partie de la valeur : on utilise donc la fonction `split()` pour séparer les deux parties de la valeur.
- Une fois récupéré, nous allons vérifier la validité de ce token, à l'aide de la méthode `verify()` de jwt. On lui passe également la clé secrète déterminée en amont, lors de la réponse envoyée pour accéder à la page de connexion.
- Nous récupérons l'id de l'utilisateur dans le token décodé, et le rajoutons dans l'objet `resquest` pour que les routes puissent l'exploiter.
- Enfin, on rajoute la fonction `next()` afin que le middleware suivant de la route puisse être exécuté.
- Lors de l'intégration de ce middleware à nos routes, il faut bien entendu importer ce middleware mais surtout le positionner avant les controleurs, pour exécuter en premier l'authentification. Si l'authentification a échoué, alors le catch renvoi le message d'erreur et le prochain middleware ne s'exécute pas.

# Partie 4 - Ajoutez une gestion des fichiers utilisateur sur l'application

## Acceptez les fichiers entrants avec multer

### Configurez le middleware de gestion des fichiers

Installer Multer (package de gestion de fichiers) :

```bash
$ npm install multer
```

[Voir la doc](https://github.com/expressjs/multer)

Créer un dossier `images` à la racine de l'application et créer un fichier `multer-config.js` pour créer le middleware :

```js
// middleware/multer-config.js

const multer = require('multer');

const MIME_TYPES = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/png': 'png'
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, 'images');
  },
  filename: (req, file, callback) => {
    const name = file.originalname.split(' ').join('_');
    const extension = MIME_TYPES[file.mimetype];
    callback(null, name + Date.now() + '.' + extension);
  }
});

module.exports = multer({storage: storage}).single('image');
```

Logique métier :
- On importe le paquet.
- La méthode `diskStorage()` permet de configurer la manière de stocker les fichiers sur le disque : le chemin de destination et le nom du fichier. Pour le nom des fichiers, le code supprime les espaces qui peuvent être source d'erreurs, en les remplaçant par des underscores.
- Les types MIME ([voir la doc MDN](https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types)) permettent de définir le format des fichiers approprié.
- On exporte `multer()` à qui on fournit en argument l'objet de configuration. La méthode `single()`  crée un middleware qui capture les fichiers d'un certain type (passé en argument), et les enregistre au système de fichiers du serveur à l'aide du storage configuré.

## Modifiez les routes pour prendre en compte les fichiers
Le format de la requête change car elle contient un fichier (form-data : un objet sous la forme d'une chaîne de caractères).

Avec une image :
![Format de la requête HTTP avec une image](img/OCR-Nodejs_Expressjs_Mongodb/request_http_with_image.png)

Sans image :
![Format de la requête HTTP sans une image](img/OCR-Nodejs_Expressjs_Mongodb/request_http_without_image.png)


### Modifiez la route POST

On ajoute le middleware `multer-config.js` et on l'ajoute à nos routes dans `routes/stuff.js`, entre l'authentification et le middleware associé à la méthode http (l'authentification préalable est essentielle pour protéger les routes). Si nous plaçions `multer` avant l'authentification, alors les images issuent d'une requête non authentifiée seraient enregistrées sur le serveur.

```js
// routes/stuff.js

const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

const stuffCtrl = require('../controllers/stuff');

router.get('/', auth, stuffCtrl.getAllThings);
router.post('/', auth, multer, stuffCtrl.createThing);
router.get('/:id', auth, stuffCtrl.getOneThing);
router.put('/:id', auth, stuffCtrl.modifyThing);
router.delete('/:id', auth, stuffCtrl.deleteThing);

module.exports = router;
```

 En ajoutant `multer`, on prend en compte le fait que le format de la requête a changé. On va donc gérer le middleware `createThing` d'une nouvelle façon :

```js
// controllers/stuff.js

exports.createThing = (req, res, next) => {
    const thingObject = JSON.parse(req.body.thing);
    delete thingObject._id;
    delete thingObject._userId;
    const thing = new Thing({
        ...thingObject,
        userId: req.auth.userId,
        imageUrl: `${req.protocol}://${req.get("host")}/images/${
            req.file.filename
        }`,
    });
    thing
        .save()
        .then(() => {
            res.status(201).json({ message: "Objet enregistré !" });
        })
        .catch((error) => {
            res.status(400).json({ error });
        });
};
```

- On doit parser l'objet `req` à l'aide de la méthode `parse()` ([Voir la doc MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse)), car l'objet `req` est envoyé au format JSON, mais sous forme de caractères désormais. La méthode `parse()` permet donc d'obtenir un objet JSON exploitable.
- Ensuite, nous devons supprimer deux champs :
    - l'`id` de l'objet car il sera généré automatiquement par mongoDB (comme déjà vu précédemment)
    - le _userId car nous pouvons faire confiance au token du client, mais jamais au client lui-même.
- Nous pouvons alors créer l'objet en l'instanciant comme précédemment, avec le _userId qui vient du middleware `auth.js`.
- Générer l'URL de l'image car `multer` ne nous fournit que le nom du fichier.
- Enfin, on enregistre l'objet en base de données à l'aide de la méthode `save()`.

La requête vers le répertoire image n'est pas encore gérée : il faut donc ajouter une route pour que le serveur puisse gérer cette requête.

Express nous fournit le middleware `static` ([voir la doc Express](https://expressjs.com/en/starter/static-files.html)), récupérer le répertoire dans lequel s'execute le serveur et le concaténer avec le répertoire `images`. Dans un premier temps, nous devons importer l'objet `path` du serveur :

```js
// app.js

const path = require('path');
```
Puis utiliser cet objet pour construire le chemin du dossier image en argument de la méthode `static` fournit pas Express :

```js
// app.js

app.use('/images', express.static(path.join(__dirname, 'images')));
```

### Modifiez la route PUT

De la même manière, on ajoute `multer` pour la méthode `put()` :

```js
// routes/stuff.js

router.put('/:id', auth, multer, stuffCtrl.modifyThing);
```
Et nous devons modifier le middleware en conséquence.

Comme vu précédemment, le format de la requête est différent suivant la présence ou l'absence d'un fichier. Il faut donc gérer les deux cas de figure lors de la récupération de l'objet dans la constante `tingObject`.

```js
// controllers/stuff.js

exports.modifyThing = (req, res, next) => {
    const thingObject = req.file
        ? {
              ...JSON.parse(req.body.thing),
              imageUrl: `${req.protocol}://${req.get("host")}/images/${
                  req.file.filename
              }`,
          }
        : { ...req.body };

    delete thingObject._userId;
    Thing.findOne({ _id: req.params.id })
        .then((thing) => {
            if (thing.userId != req.auth.userId) {
                res.status(401).json({ message: "Not authorized" });
            } else {
                Thing.updateOne(
                    { _id: req.params.id },
                    { ...thingObject, _id: req.params.id }
                )
                    .then(() =>
                        res.status(200).json({ message: "Objet modifié!" })
                    )
                    .catch((error) => res.status(401).json({ error }));
            }
        })
        .catch((error) => {
            res.status(400).json({ error });
        });
};
```
- En cas de présence d'un fichier, le champ `file` sera présent, et on parse donc l'objet `thing` du corps de la requête, et en définissant l'URL de l'image par son champ `imageUrl`. Sinon, on récupère directement le body de la requête. A part l'utilisation du ternaire, c'est ce que nous avons vu précédemment.
- On efface le `userId`, toujours pour des mesures de sécurité, car une personne ayant créé un objet pourrait très bien le réassigner à quelqu'un d'autre.
- On va chercher l'objet en base de donnée grâce aux params, et vérifier l'authentification du user à l'aide du token d'identification envoyé.
- Si l'authentification est validée, alors on utilise la méthode `updateOne()` pour mettre à jour l'objet en base de données ([Voir la doc MongoDB](https://www.mongodb.com/docs/manual/reference/method/db.collection.updateOne/)) : en premier argument le filtre, en deuxème la mise à jour (avec l'identifiant de l'objet qui doit rester inchangé).
- Cette méthode retourne une promesse que l'on gère comme précédemment.

## Développez la fonction delete du back-end

Il faudra s'assurer de l'authentification de la personne ainsi que de la suppression, à la fois dans la base de données, mais aussi de l'image dans le dossier `images`.

Commencer par l'import du package `fs` (pour File System) qui donne accès aux fonctions permettant de modifier le système de fichiers.

```js
// controllers/stuff.js

const fs = require('fs');
```

Ensuite, nous pouvons modifier la logique métier dans le controleur :

```js
// controllers/stuff.js

exports.deleteThing = (req, res, next) => {
   Thing.findOne({ _id: req.params.id})
       .then(thing => {
           if (thing.userId != req.auth.userId) {
               res.status(401).json({message: 'Not authorized'});
           } else {
               const filename = thing.imageUrl.split('/images/')[1];
               fs.unlink(`images/${filename}`, () => {
                   Thing.deleteOne({_id: req.params.id})
                       .then(() => { res.status(200).json({message: 'Objet supprimé !'})})
                       .catch(error => res.status(401).json({ error }));
               });
           }
       })
       .catch( error => {
           res.status(500).json({ error });
       });
};
```

- On va chercher l'objet passé dans les params, en base de données, grace à la méthode `findOne` du package Mongoose.
- Cette méthode retourne une promesse,. On doit alors vérifier que le champ `uderId` est bien le même que celui passé dans le token.
- Si l'authentification est validée, alors on récupère le nom du fichier, et à l'aide de la méthode `unlink()` du package fs (voir doc sur [geeksforgeeks](https://www.geeksforgeeks.org/node-js-fs-unlink-method/)), on supprime le fichier et on lui passe en deuxième argument la fonction callback qui supprimer en base de données à l'aide de la méthode `deleteOne()` fournit par Mongoose.

# Annexes (bonus perso à venir)

## Installation de Swagger pour Express
https://www.npmjs.com/package/swagger-ui-express

## Les méthodes de requêtes HTTP
https://developer.mozilla.org/fr/docs/Web/HTTP/Methods


## Code de réponses HTTP
https://developer.mozilla.org/fr/docs/Web/HTTP/Status

Les réponses informatives (100 - 199)
Les réponses de succès (200 - 299)
Les messages de redirection (300 - 399)
Les erreurs du client (400 - 499)
Les erreurs du serveur (500 - 599)
