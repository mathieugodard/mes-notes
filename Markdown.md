2020-11-16

[[_TOC_]]

# Table des matières

Pour afficher sur Gitlab :

```md
[[_TOC_]]
```

Pour le logiciel `Joplin` :

```md
[TOC]
```

Ce qui donne... la table des matières au début de ce document !

# Séparateur horizontal

```md
---
```

Ce qui donne :

---

# Code block

\`\`\`javascript  
const express = require("express");  
const app = express();  
const mongoose = require("mongoose");  
const path = require('path');

const postRoutes = require('./routes/postRoutes');  
const userRoutes = require('./routes/userRoutes');  
\`\`\`

donne :

```javascript
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const path = require("path");

const postRoutes = require("./routes/postRoutes");
const userRoutes = require("./routes/userRoutes");
```

# Code line

Une phrase avec une ligne de commande, le nom d'une fonction, un chemin ou un petit bout de code comme \`path()\` intégré dedans.

donne :

Une phrase avec une ligne de commande, le nom d'une fonction, un chemin ou un petit bout de code comme `path()` intégré dedans.

# Les titres

```md
# Titre H1
## Titre H2
### Titre H3
#### Titre H4
##### Titre H5
###### Titre H6
```

donne :

# Titre H1

## Titre H2

### Titre H3

#### Titre H4

##### Titre H5

###### Titre H6

# Emphasis

```md
_This text will be italic_  
_This will also be italic_  
**This text will be bold**  
**This will also be bold**  
_You **can** combine them_
~~The world is flat.~~
```

donne :

_This text will be italic_  
_This will also be italic_  
**This text will be bold**  
**This will also be bold**  
_You **can** combine them_  
~~The world is flat.~~

# Les tableaux

Pour générer des tableaux : https://www.tablesgenerator.com/markdown_tables#

# Listes non ordonnées

```md
-   Item 1
-   Item 2
    -   Item 2a
    -   Item 2b
```

donne :

-   Item 1
-   Item 2
    -   Item 2a
    -   Item 2b

# Listes ordonnées

```md
1. Item 1
2. Item 2
3. Item 3
    1. Item 3a
    2. Item 3b
```

donne :

1. Item 1
2. Item 2
3. Item 3
    1. Item 3a
    2. Item 3b

# Liens

```md
[Framasoft](https://framasoft.org/fr/)
```

donne :

[Framasoft](https://framasoft.org/fr/)

# Images

```
![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)
```

## Insérer une image simple

```md
![logo](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)
```

donne :

![logo](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)

## Image cliquable vers un autre lien

```md
[![logo](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)](https://framasoft.org)
```

donne :

[![logo](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)](https://framasoft.org)

## Image avec légende

```md
[![logo](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)_Source Wikipédia_](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)
```

donne :

[![logo](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)_Source Wikipédia_](https://upload.wikimedia.org/wikipedia/commons/archive/b/bb/20150910113353%21Framasoft_Logo.svg)

# Cases à cocher

```md
-   [ ] Élément de liste
-   [ ] Élément de liste
-   [x] Élément de liste
```

donne :

-   [ ] Élément de liste
-   [ ] Élément de liste
-   [x] Dernier élément

# Citations

```md
> La route est longue mais la voie est libre
```

donne :

> La route est longue mais la voie est libre

# Notes de bas de page

```
Here's a sentence with a footnote. [^1]
[^1]: This is the footnote.
```

donne :

Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.

# Saut de page

Comme Markdown accepte le HTML et les CSS simples, ajoutez simplement cette ligne à l'endroit où vous souhaitez forcer le saut de page.

```html
<div style="page-break-after: always;"></div>
```

Si votre éditeur Markdown rencontre des difficultés pour exporter PDF correctement, essayez d’abord d’exporter au format HTML, puis ouvrez-le avec votre navigateur et imprimez-le au format PDF.

# Important (balise `<em>`)

```md
_TEXTE_
```

ou

```md
*TEXTE*
```

donne : _TEXTE_

# Très important (balise `<strong>`)

```md
**TEXTE**
```

donne : **TEXTE**

# Souligné

```md
<ins>TEXTE</ins>
```

donne : <ins>TEXTE</ins>
