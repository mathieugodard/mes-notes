Réalisation du cours d'Open Class Rooms : [https://openclassrooms.com/fr/courses/7471261-debutez-avec-angular](https://openclassrooms.com/fr/courses/7471261-debutez-avec-angular)

SOMMAIRE

<!-- TOC -->

- [Partie 1 - Préparez le terrain pour Angular](#partie-1---pr%C3%A9parez-le-terrain-pour-angular)
    - [Découvrez le framework Angular](#d%C3%A9couvrez-le-framework-angular)
    - [Installez les dépendances](#installez-les-d%C3%A9pendances)
    - [Construisez une application Angular avec le CLI](#construisez-une-application-angular-avec-le-cli)
- [Partie 2 - Construisez des components](#partie-2---construisez-des-components)
    - [Construisez votre premier component](#construisez-votre-premier-component)
    - [Affichez des données](#affichez-des-donn%C3%A9es)
        - [Ajoutez des données data-binding](#ajoutez-des-donn%C3%A9es-data-binding)
        - [Affichez du contenu avec la string interpolation](#affichez-du-contenu-avec-la-string-interpolation)
        - [Dynamisez des données avec l'attribute binding](#dynamisez-des-donn%C3%A9es-avec-lattribute-binding)
        - [Ajoutez un peu de style](#ajoutez-un-peu-de-style)
    - [Réagissez aux événements event-binding](#r%C3%A9agissez-aux-%C3%A9v%C3%A9nements-event-binding)
    - [Ajoutez des propriétés personnalisées](#ajoutez-des-propri%C3%A9t%C3%A9s-personnalis%C3%A9es)
        - [Créez une classe FaceSnap](#cr%C3%A9ez-une-classe-facesnap)
        - [Ajoutez des propriétés personnalisées](#ajoutez-des-propri%C3%A9t%C3%A9s-personnalis%C3%A9es)
- [Partie 3 - Structurez un document avec des directives](#partie-3---structurez-un-document-avec-des-directives)
    - [Conditionnez l'affichage des éléments](#conditionnez-laffichage-des-%C3%A9l%C3%A9ments)
    - [Affichez des listes](#affichez-des-listes)
    - [Ajoutez du style dynamique](#ajoutez-du-style-dynamique)
    - [Mettez de la classe](#mettez-de-la-classe)
- [Partie 4 - Modifiez l'affichage des données avec les pipes](#partie-4---modifiez-laffichage-des-donn%C3%A9es-avec-les-pipes)
    - [Changez la casse](#changez-la-casse)
    - [Formatez les dates](#formatez-les-dates)
    - [Formatez les chiffres](#formatez-les-chiffres)
- [Partie 5 - Améliorez la structure de votre application avec les services et le routing](#partie-5---am%C3%A9liorez-la-structure-de-votre-application-avec-les-services-et-le-routing)
    - [Partagez des données avec les Services](#partagez-des-donn%C3%A9es-avec-les-services)
    - [Centralisez votre logique avec les Services](#centralisez-votre-logique-avec-les-services)
    - [Passez en SPA avec le routing](#passez-en-spa-avec-le-routing)
    - [Passez d'une route à l'autre](#passez-dune-route-%C3%A0-lautre)
    - [Activez les routes avec ActivatedRoute](#activez-les-routes-avec-activatedroute)

<!-- /TOC -->

# Partie 1 - Préparez le terrain pour Angular

## 1. Découvrez le framework Angular

-   Le framework Angular utilise le HTML, le SCSS et le TypeScript.
-   Le TypeScript est un langage qui ajoute des syntaxes au JavaScript, notamment pour le typage strict.

## 2. Installez les dépendances

Installer le CLI d'Angular :

```bash
$ sudo npm i -g @angular/cli
```

Afficher les détails de la version installée :

```bash
$ ng v

? Would you like to enable autocompletion? This will set up your terminal so pressing TAB while typing Angular CLI commands will show possible options and autocomplete arguments. (Enabling autocompletion will modify configuration files in your home directory.) Yes
Appended `source <(ng completion script)` to `/home/mathieu/.bashrc`. Restart your terminal or run the following to autocomplete `ng` commands:

    source <(ng completion script)
? Would you like to share anonymous usage data about this project with the Angular Team at Google under Google’s Privacy Policy at https://policies.google.com/privacy. For more details and how to change this setting, see https://angular.io/analytics. No
Global setting: disabled
Local setting: No local workspace configuration file.
Effective status: disabled

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 14.1.3
Node: 16.17.0
Package Manager: npm 8.13.2
OS: linux x64

Angular:
...

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1401.3 (cli-only)
@angular-devkit/core         14.1.3 (cli-only)
@angular-devkit/schematics   14.1.3 (cli-only)
@schematics/angular          14.1.3 (cli-only)
```

## 3. Construisez une application Angular avec le CLI

Créer une application Angular avec le CLI :

```bash
$ ng new snapface --style=scss --skip-tests=true
# snapface est le nom de l'application
# le flag --style=scss indique que nous utiliseront le scss
# le flag --skip-tests=true car pas de tests unitaires sur ce cours
? Would you like to add Angular routing? No
# Nous ajouterons le routing à la main dans la suite du cours
CREATE snapface/README.md (1062 bytes)
CREATE snapface/.editorconfig (274 bytes)
CREATE snapface/.gitignore (548 bytes)
CREATE snapface/angular.json (3695 bytes)
CREATE snapface/package.json (1039 bytes)
CREATE snapface/tsconfig.json (863 bytes)
CREATE snapface/.browserslistrc (600 bytes)
CREATE snapface/karma.conf.js (1425 bytes)
CREATE snapface/tsconfig.app.json (287 bytes)
CREATE snapface/tsconfig.spec.json (333 bytes)
CREATE snapface/.vscode/extensions.json (130 bytes)
CREATE snapface/.vscode/launch.json (474 bytes)
CREATE snapface/.vscode/tasks.json (938 bytes)
CREATE snapface/src/favicon.ico (948 bytes)
CREATE snapface/src/index.html (294 bytes)
CREATE snapface/src/main.ts (372 bytes)
CREATE snapface/src/polyfills.ts (2338 bytes)
CREATE snapface/src/styles.scss (80 bytes)
CREATE snapface/src/test.ts (749 bytes)
CREATE snapface/src/assets/.gitkeep (0 bytes)
CREATE snapface/src/environments/environment.prod.ts (51 bytes)
CREATE snapface/src/environments/environment.ts (658 bytes)
CREATE snapface/src/app/app.module.ts (314 bytes)
CREATE snapface/src/app/app.component.scss (0 bytes)
CREATE snapface/src/app/app.component.html (23083 bytes)
CREATE snapface/src/app/app.component.ts (213 bytes)
✔ Packages installed successfully.
    Directory is already under version control. Skipping initialization of git.
```

Aller dans le dossier de l'application :

```bash
$ cd snapface
```

Lancer le serveur de développement :

```bash
$ ng serve
```

NDLR :
Pour ma part, j'ai eu cette erreur :

```bash
Watchpack Error (watcher): Error: ENOSPC: System limit for number of file watchers reached, watch '/home/mathieu/code/openclassrooms/debutez-avec-angular/snapface/node_modules/webpack-dev-server/client/modules/logger'
Watchpack Error (watcher): Error: ENOSPC: System limit for number of file watchers reached, watch '/home/mathieu/code/openclassrooms/debutez-avec-angular/snapface/node_modules/webpack-dev-server/client/modules'
```

Temporairement, on peut résoudre ce problème en lançant cette commande :

```bash
$ ng serve --watch --live-reload --poll 2000
✔ Browser application bundle generation complete.

Initial Chunk Files   | Names         |  Raw Size
vendor.js             | vendor        |   1.73 MB |
polyfills.js          | polyfills     | 315.29 kB |
styles.css, styles.js | styles        | 207.83 kB |
main.js               | main          |  47.73 kB |
runtime.js            | runtime       |   6.51 kB |

                      | Initial Total |   2.29 MB

Build at: 2022-08-20T17:27:08.727Z - Hash: 964e827600cf3005 - Time: 8584ms

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **


✔ Compiled successfully.
```

Le serveur de l'application redémarre à chaque nouvelle modification du code.
Mais pour résoudre le problème sur du long terme, il faut taper la commande :

```bash
$ echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

Source : https://github.com/gatsbyjs/gatsby/issues/11406#issuecomment-458769756

NDLR : demander à @Enzo s'il connait ce souci pour approfondir la compréhension de cette résolution.

Le serveur lancé, on peut alors visiter la page http://localhost:4200/ pour voir l'application par défaut d'Angular.

A ce stade, on observe beaucoup de fichiers de configurations.  
Nous allons travailler principalement dans le dossier `src`, où se trouve le point d'entrée de l'application `index.html`, qui contient l'app component (le composant de l'application). Ce composant est définit dans le dossier `component` qui possède 4 fichiers.

Le composant `<app-root></app-root>` est déclaré dans le fichier `app.module.ts` (comme tous les components). L'application sera alors composée d'une arborescence de composants, qui auront systématiquement 3 fichiers :

-   un fichier `html` pour la structure
-   un fichier `scss` pour le style
-   un fichier `ts` pour la logique

L'idée est d'avoir des composants réutilisable en ayant pour objectif de découper son application en composants de tailles "raisonnables", pour la lisibilité et la logique du code.

# Partie 2 - Construisez des components

## 1. Construisez votre premier component

Le CLI d'Angular permet d'automatiser la création d'un component (entre autre...). Lorsque nous executons :

```angular
$ ng generate component face-snap CREATE
src/app/face-snap/face-snap.component.scss (0 bytes) CREATE
src/app/face-snap/face-snap.component.html (24 bytes) CREATE
src/app/face-snap/face-snap.component.ts (287 bytes) UPDATE
src/app/app.module.ts (406 bytes)
```

Nous créons un component nommé `FaceSnapComponent` dans un nouveau dossier appelé `face-snap`. Le 'S' majuscule est généré par le trait d'union, et le 'Component' est ajouté automatiquement par le CLI.

Ainsi, l'ensemble des fichiers sont générés pour un component de base, et le component est ajouté dans l'application grâce à la mise à jour du fichier `app.module.ts` (déclaration) afin qu'il soit utilisable à n'importe quel endroit de l'application.

Dans un but pédagogique, nous allons supprimer un peu de logique dans le fichier `FaceSnapComponent.ts` généré, en supprimer l'import et l'utilisation du module `OnInit`. On passe alors de ça :

```bash
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
```

à ça :

```bash
import { Component } from '@angular/core';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent {}
```

`@Component` est ce que l'on appelle à décorateur, et permet d'utiliser tous les comportements de la classe `Component` importés du package `@angular/core`.

Il nous reste plus qu'à utiliser le sélecteur `<app-face-snap></app-face-snap>` dans le composant parent (`app.component.html`), et apprécier l'affichage de ce texte de toute beauté :

```html
<!--  face-snap/face-snap.component.html -->

<p>Pouët !</p>
```

On peut utiliser le component autant de fois que nous le souhaitons (réutilisable).

```html
<!-- app.component.html  -->

<app-face-snap></app-face-snap>
<app-face-snap></app-face-snap>
<app-face-snap></app-face-snap>
```

Ce qui donne :

![Résultat](img/OCR-Angular-1-Debutez-avec-Angular/Multiple_utilisations_du_composant.png)

NDLR : à chaque fois qu'on appelle le component, c'est une nouvelle instance. Chaque instance est indépendante des autres. Par exemple, si on clique sur un bouton d'un des component, changeant ce component d'une manière ou d'une autre, les autres components ne seront pas affectés.

## 2. Affichez des données

### Ajoutez des données (data-binding)

Pour afficher des données dynamiques, nous allons voir la string interpolation et la liaison par attribut (ou attribute binding). Pour le moment, les données ne viennent pas d'un serveur mais viendront de données "en dur".

On ajoute des propriétés liées aux photos partagées. L'IDE souligne une erreur d'initialisation :

![Erreur signalée par l IDE](img/OCR-Angular-1-Debutez-avec-Angular/Initialisation_de_la_propriete.png)

On associe le nom de la propriété à son type (puisqu'on est en typescript). On ajoute un opérateur d'assertion non-nul "!" (appelé aussi le "bang") parceque typescript est en mode strict, et il alerte sur le fait que la propriété n'est pas initialisée. Ce bang permet de promettre à typescript qu'on va bien l'initialiser.

https://www.typescriptlang.org/docs/handbook/2/classes.html#--strictpropertyinitialization

Dans une classe typescript, on déclare d'abord les propriétés, puis on initialise en implémentant l'interface `OnInit` et à l'aide de sa méthode `ngOnInit()`. D'ailleurs, c'est cette interface que nous avons supprimé juste avant : elle est appelée au moment de l'instanciation d'un component.

https://angular.io/api/core/OnInit

Ce qui donne :

```js
// face-snap.component.ts

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {
  title!: string;
  description!: string;
  createdDate!: Date;
  snaps!: number;

  ngOnInit() {
    this.title = 'Harry Tuttle';
    this.description = 'Technicien chauffagiste clandestin';
    this.createdDate = new Date();
    this.snaps = 6;
  }
}
```

### Affichez du contenu avec la string interpolation

Afficher les valeurs de ces variables dans le template : le string interpolation s'utilise avec les doubles accolades `{{ nameProperty }}`.

Exemple :

```html
<!-- face-snap.component.html -->

<h2>{{ title }}</h2>
<p>Mis en ligne le {{ createdDate }}</p>
<p>{{ description }}</p>
<p>🤟 {{ snaps }}</p>
```

Ce qui donne :
![Affichage avec la string interpolation](img/OCR-Angular-1-Debutez-avec-Angular/String_interpolation.png)

### Dynamisez des données avec l'attribute binding

L'attribute binding permet de passer les propriétés à des attributs du component. Comme avant, on va déclarer puis initialiser la propriété `imageUrl` :

```ts
export class FaceSnapComponent implements OnInit {
    imageUrl!: string;

    ngOnInit() {
        this.imageUrl = "./assets/img/harry_tuttle.jpg";
    }
}
```

et au lieu d'utiliser le string interpolation, nous allons utiliser l'attribute binding pour nous affranchir du type de la donnée. Pour une image, l'attribut `src` nécessite qu'on lui passe une propriété de type string. Ici, comme les propriété `imageUrl` et `title` sont des chaînes de caractères, ça fonctionnerait bien. Mais que se passerait-il si nous devions lier une donnée de type number ? Cela ne fonctionnerait pas ! Donc, voici la syntaxe à utiliser pour lier la propriété avec le bon typage de données :

```html
<!-- face-snap.component.html-->

<h2>{{ title }}</h2>
<p>Mis en ligne le {{ createdDate }}</p>
<img [src]="imageUrl" [alt]="title" />
<p>{{ description }}</p>
<p>🤟 {{ snaps }}</p>
```

Les crochets permettent de lier l'attribut à la propriété tout en gardant le bon typage.

### Ajoutez un peu de style

On ajoute le stype Roboto dans le point d'entrée de l'application

```html
<!-- index.html > head -->

<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link
    href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap"
    rel="stylesheet"
/>
```

On applique cette typo à l'ensemble du site :

```scss
// style.scss

* {
    font-family: Roboto, Helvetica, sans-serif;
}
```

On ajoute la classe au template du component :

```html
<!-- face-snap.component.html -->

<div class="face-snap-card">
    <h2>{{ title }}</h2>
    <p>Mis en ligne le {{ createdDate }}</p>
    <img [src]="imageUrl" [alt]="title" />
    <p>{{ description }}</p>
    <p>🤌 {{ snaps }}</p>
</div>
```

et enfin on applique le style au composant lui-même :

```scss
// snap-face.component.scss

.face-snap-card {
    width: 15%;
    img {
        width: 100%;
    }
}
```

## 3. Réagissez aux événements (event-binding)

L'`event-binding` est le sens "inverse" du `data-binding` : du template vers le typescript. Nous allons lier des méthodes typescript à des événements HTML. Le plus simple des événements, c'est un clic sur un bouton : c'est ce que nous allons voir de suite !

L'idée c'est de pouvoir modifier la propriété en cliquant sur le bouton snaps.

Après avoir créé le bouton `Oh Snaps!` pour liker le snap, on doit créer une méthode (le comportement du bouton) qui doit être déclenché par l'événement `click` sur ce boutton. On va donc créer une méthode pour incrémenter le nombre de snaps.

```ts
// face-snap.component.ts

onAddSnap() {
  this.snaps++;
}
```

Et pour lier la méthode au click, on met entre parenthèses l'événement et on le lie à la méthode :

```html
<!-- face-snap.component.html -->

<p>
    <button (click)="onAddSnap()">Oh snaps !</button>
    🤟 {{ snaps }}
</p>
```

Désormais, lorsqu'on clique sur le bouton, on incrémente la propriété `snaps`.

Pour l'exercice, il faut pouvoir modifier le texte et incrémenter (+1) / désincrémenter (-1) le nombre de snaps en fonction des clics de l'utilisateur (pas plus de 1). J'ai créé une propriété booléenne `snaped` et une propriété string `snapButtonText` pour lier la méthode au DOM :

```ts
// snap-face.compoment.ts

export class FaceSnapComponent implements OnInit {
    snaped!: boolean;
    snapButtonText!: string;

    ngOnInit() {
        this.snaped = false;
        this.snapButtonText = "Oh snaps!";
    }

    toggleSnap() {
        if (!this.snaped) {
            this.snaps++;
            this.snaped = !this.snaped;
            this.snapButtonText = "Oops unsnap!";
        } else {
            this.snaps--;
            this.snaped = !this.snaped;
            this.snapButtonText = "Oh snaps!";
        }
    }
}
```

et

```html
<!-- snap-face.component.html -->

<p>
    <button (click)="toggleSnap()">{{ snapButtonText }}</button>
    🤟 {{ snaps }}
</p>
```

Dans le cours, le choix a été fait de n'avoir qu'un propriété de type `string` pour le texte du boutton, et que ce texte soit la variable conditionnelle pour l'incrémentation ou la désincrémentation. Le code est plus concis.

## 4. Ajoutez des propriétés personnalisées

Pour le moment, si nous voulons utiliser 3 composants avec des données différentes, on est obligé d'initialiser les données sur chaque component (les données sont codés "en dur"). Pour être réellement réutilisable, le component doit obtenir les données depuis le composant parent.

L'intérêt est donc de pouvoir passer des données différentes à chaque instance de component : on va donc ajouter des propriétés personnalisées à nos component pour pouvoir les injecter via le component parent. Autrement dit, notre `AppComponent` va centraliser toutes les données et les injecter à chaque instalce de notre component `FaceSnapComponent`. Pour celà, on va créer une classe modèle.

### Créez une classe FaceSnap

Créons notre premier type personnalisé

Créer un dossier `models` dans le dossier `app` et créer un fichier `facesnap.models.ts`. Nous allons créer notre classe FaceSnap qui comportera toutes les propriétés à injecter. Elles nous intéressent toutes, sauf le boutton car il sera toujours le même (ne fera pas partie des données injectées).

Si nous créons une classe de manière classique :

```ts
export class FaceSnap {
    title: string;
    description: string;
    createdDate: Date;
    snaps: number;
    imageUrl: string;

    constructor(
        title: string,
        description: string,
        imageUrl: string,
        createdDate: Date,
        snaps: number
    ) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.createdDate = createdDate;
        this.snaps = snaps;
    }
}
```

Mais tout ceci est très verbeux. Typescript nous permet de réduire ce code en utilisant le modificateur (en anglais "modifier", voir la [doc](https://www.typescriptlang.org/docs/handbook/2/classes.html) à ce sujet) `public` dans le constructeur, ce qui nous permet de supprimer les déclarations de propriétés et l'initialisation :

```ts
export class FaceSnap {
    constructor(
        public title: string,
        public description: string,
        public imageUrl: string,
        public createdDate: Date,
        public snaps: number
    ) {}
}
```

Le type personnalisé prêt, il faut désormais faire en sorte que `FaceSnapComponent` puisse recevoir les données du parent `AppComponent`.

### Ajoutez des propriétés personnalisées

Tout d'abord, dans mon component `face-snap.component.ts`, je vais importer une propriété `faceSnap` de type `FaceSnap` (celle que l'on vient de créer) à l'aide du décorateur `@Input` que nous devons également importer ! Cette propriété sera notre attribut qui va recevoir les données du component parent.

```ts
import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {
  @Input() faceSnap!: FaceSnap;
  [.............]
}
```

Désormais, il faut créer de la donnée à passer à cet attribut. On créé alors une propriété nommée `mySnap` qui aura elle aussi le type `FaceSnap`. Ensuite, on initialise la propriété  avec la méthode `ngOnInit()`, simplement en créant une instance de la classe `FaceSnap` (importée du modèle `face-snap.model`). Mon component est alors prêt à recevoir la donnée : la propriété `mySnap`.

```ts
import { Component, OnInit } from '@angular/core';
import { FaceSnap } from './models/face-snap.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  mySnap!: FaceSnap;

  ngOnInit() {
    this.mySnap = new FaceSnap(
      'Harry Tuttle', 
      "Dans le monde totalitaire de Brazil, Archibald Tuttle est un plombier chauffagiste dissident (« plombier vengeur ») qui intervient illégalement chez les gens pour réparer leurs climatisations. Ses amis l'appellent simplement Harry. Il fait irruption chez Sam Lowry, le personnage central du film, armé d'un Walther P38, dans le but de lui réparer son climatiseur défectueux.",
      new Date(),
      0,
      "./assets/img/harry_tuttle.jpg",
      false
    )
  }
}

```

On passe donc à l'attribut `faceSnap` la propriété `mySnap` qui contient la donnée créée dans le component parent `AppComponent`.

```html
<app-face-snap [faceSnap]="mySnap"></app-face-snap>
```

Enfin, pour que les données s'affichent, il faut modifier le template du component, car ce ne sont plus les propriétés du component que l'on utilise dans les doubles accolades, mais les propriétés de la propriété `faceSnap` de type `FaceSnap`. On utilise donc la synthaxe pointée.

```html
<div class="face-snap-card">
  <h2>{{ faceSnap.title }}</h2>
  <p>Mis en ligne le {{ faceSnap.createdDate }}</p>
  <img [src]="faceSnap.imageUrl" [alt]="faceSnap.title" />
  <p>{{ faceSnap.description }}</p>
  <p>
    <button (click)="toggleSnap()">{{ snapButtonText }}</button>
    🤟 {{ faceSnap.snaps }}
  </p>
</div>
```
NDLR : la méthode sur le boutton nécessite également de modifier les propriété utilisées :

```ts
toggleSnap() {
    if(!this.faceSnap.snaped) {
      this.faceSnap.snaps++;
      this.faceSnap.snaped = !this.faceSnap.snaped;
      this.snapButtonText = 'Oops unsnap!'
    } else {
      this.faceSnap.snaps--;
      this.faceSnap.snaped = !this.faceSnap.snaped;
      this.snapButtonText = 'Oh snaps!'
    }
  }
```

# Partie 3 - Structurez un document avec des directives

## 1. Conditionnez l'affichage des éléments

## 2. Affichez des listes

## 3. Ajoutez du style dynamique

## 4. Mettez de la classe

# Partie 4 - Modifiez l'affichage des données avec les pipes

## 1. Changez la casse

## 2. Formatez les dates

## 3. Formatez les chiffres

# Partie 5 - Améliorez la structure de votre application avec les services et le routing

## 1. Partagez des données avec les Services

## 2. Centralisez votre logique avec les Services

## 3. Passez en SPA avec le routing

## 4. Passez d'une route à l'autre

## 5. Activez les routes avec ActivatedRoute
