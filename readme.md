Ce dépôt est un dépôt personnel de notes concernant le code.
OCR = Open Class Rooms

Liste des notes à mettre à jour sur le repo

# API
https://code-garage.fr/blog/qu-est-ce-qu-une-api-rest/amp/

# Serverless


# Open graph
https://fr.oncrawl.com/referencement/tout-ce-que-vous-devez-savoir-sur-lopen-graph/
https://ogp.me/#types

# Accessibilité
https://www.lunaweb.fr/blog/comment-concevoir-un-site-adapte-aux-seniors/

# JSON Wen Token
https://github.com/auth0/node-jsonwebtoken
https://self-issued.info/docs/draft-ietf-oauth-json-web-token.html
https://github.com/auth0/node-jsonwebtoken
