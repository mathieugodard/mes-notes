Issu du cours d'OpenClassRooms : https://openclassrooms.com/fr/courses/7693926-devenez-developpeur-agile

[[_TOC_]]

# Manifeste pour le développement Agile de logiciels
http://agilemanifesto.org/iso/fr/manifesto.html

Nous découvrons comment mieux développer des logiciels par la pratique et en aidant les autres à le faire. Ces expériences nous ont amenés à valoriser :

- **Les individus et leurs interactions** plus que les processus et les outils
- **Des logiciels opérationnels** plus qu’une documentation exhaustive
- **La collaboration avec les clients** plus que la négociation contractuelle
- **L’adaptation au changement** plus que le suivi d’un plan

Nous reconnaissons la valeur des seconds éléments, mais privilégions les premiers.

# Les étapes de l'agilité

![Schéma](img/schema_agility.png)

La méthode SCRUM traduit l'agilité en proposant une organisation du projet par sprints.

1. Préparer le sprint
    1. Etat des lieux (product backlog, ou tickets) : liste des fonctionnalités, bugs, ...
    2. Précisions sur l'aspect technique
    3. Planning du sprint

2. Réaliser le sprint : réalisation des tickets dans un temps généralement compris entre 1 à 4 semaines.

3. Faire le bilan du sprint (sprint review) et réitérer

> L’agilité repose sur le BDD (Behavior Driven Development), c'est-à-dire une collaboration entre les développeurs, les commerciaux et toutes les parties prenantes du projet. Cette collaboration encourage une communication claire autour du produit, qui s'appuie sur des exemples concrets d’utilisation. Le but de cette démarche est de favoriser une compréhension partagée du comportement que doit avoir le logiciel. ([Source](https://openclassrooms.com/fr/courses/7693926-devenez-developpeur-agile/7860661-cernez-les-enjeux-d-une-organisation-agile#/id/r-7862112))

**Note perso** : finalement, l'agilité c'est le travail d'équipe (communication, entraide, ...) et de courtes itérations (intégration continue CI) pour être réactif sur le projet et éviter de perdre du temps.

# Les membres de l'équipe

- Le développeur et son binôme développeur
- Le lead développeur : aide les développeurs par son expérience de développeur. Il aide à monter en compétences.
- Le product owner : ancien développeur qui définit le fonctionnement du produit, interface entre les dev et les clients. Il écrit les tickets et les sélectionne lors du sprint planning.
- Le scrum master : garantir la bonne communication entre les membres de l'équipe et éviter les conflits. C'est un facilitateur. Anime les réunions type dailys, les retrospectives de sprint ou les sprint review.

# Préparez votre Sprint

La préparation d’un Sprint repose sur différentes étapes réalisées en équipe :

- découper techniquement les tâches du Product Backlog lors du Backlog Refinement
- estimer la complexité technique ou le temps nécessaire pour traiter chaque ticket grâce au Poker Planning
- prioriser et s’engager sur le traitement d’un nombre donné de tickets pendant le Sprint Planning
- communiquer ces objectifs aux parties prenantes.

# Développez en mode agile

- Le développement agile repose sur la programmation par pairs, qui consiste à développer en binôme.
- Plusieurs méthodes de développement agile vous permettront d’obtenir un code lisible et maintenable :
    - le TDD consiste à développer un programme en commençant par les tests ;
    - le KISS consiste à développer un code simple et lisible ;
    - le refactoring consiste à améliorer le code source d’un logiciel en continu.
- Le développement agile repose également sur une communication quotidienne grâce au Daily, une réunion d’équipe où chacun évoque ses avancées et les éventuels points bloquants.
- Tout développement implémenté doit faire l’objet d’une validation collective :
    - une validation technique par l'équipe de développement lors de Code Reviews ;
    - une validation fonctionnelle par le Product Owner.

# Faites le bilan du Sprint et itérez !

- Le bilan d’un Sprint est réalisé à travers deux réunions :
    - la Rétrospective permet de réaliser un bilan humain, et d’identifier des axes d'amélioration en équipe ;
    - la Sprint Review permet de présenter les résultats aux parties prenantes, et de prendre en compte leurs retours et nouvelles demandes.
- À l’issue de la Sprint Review, les tickets restants et les User Stories doivent être réévalués en fonction des nouveaux besoins.
- Cette réévaluation est réalisée collectivement lors du Backlog Refinement suivant, marquant ainsi le début d’une nouvelle itération.